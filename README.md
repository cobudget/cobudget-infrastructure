# CoTech Cobudget Infrastructure

## Requirements

- Ansible version 2.2+
- A copy of the `vault-pass`. Obtain from joaquim@outlandish.com.
- SSH access to the `fund.coops.tech` server.

## Prepare

- Add your ssh public key to `roles/ssh/files`, and add the file to the list in 
`roles/ssh/main.yml`.

## Deploy

1. `ansible-galaxy install -r requirements.yml`
2. `ansible-playbook -i hosts server.yml`